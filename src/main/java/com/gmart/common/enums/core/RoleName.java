package com.gmart.common.enums.core;

public enum RoleName {
	ADMIN, USER, MANAGER
}
